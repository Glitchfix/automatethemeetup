#!/usr/bin/python3

######################################
# Author: Shivanjan Chakravorty
# Information extraction from the text is done by this parser
######################################
import re

replace_pattern = "\s*(.*?)\s*"
search_pattern = '\{\{\s*(.*?)\s*\}\}'


def regexFromTemplate(template, templateID):
    """
    Generate the regular expression template to be parsed

    Args:
        template: jinja template string
        templateID: Unique identifier for that particular jinja template
    Returns:
        Boolean: True if parsing template was successfully created, else False.
    """
    try:
        templateID = str(templateID)
        regexTemplate = re.sub(search_pattern, replace_pattern, template)
        entities = re.findall(search_pattern, template)[0]

        entitiesFile = open(templateID+".entities", "w+")
        entitiesFile.write(entities)
        entitiesFile.close()

        templateFile = open(templateID+".regex", 'w+')
        templateFile.write(regexTemplate)
        templateFile.close()
        
        return True
    except:
        return False
        pass


def parseTemplate(templateID, doc):
    """
    Parse the document string and extract the information

    Args:
        templateID: Unique identifier for the regex template to be parsed
        doc: The document body to be checked
    Returns:
        Boolean: True if parsed successfully, else False.
        String[]: When True, List of strings representing the extracted entities else returns None.
    """
    try:
        entitiesFile = open(templateID+".entities", "r")
        entitites = eval(entitiesFile.read())
        entitiesFile.close()

        regexFile = open(templateID+".regex", "r")
        regexTemplate = regexFile.read()
        regexFile.close()

        data = re.findall(regexTemplate, doc)[0]
        result = dict(zip(entitites, data))
        
        return True, result
    except:
        return False, None
        pass
