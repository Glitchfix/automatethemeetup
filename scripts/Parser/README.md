# Information extraction

Information extraction

```python
from parser import regexFromTemplate, parseTemplate

#Creating the template
if regexFromTemplate("RSVP for {{meetup_name}} by {{meetup_id}}",21):
    print("Success")
else:
    print("Error creating template")

#Parsing the template
err, result = parseTemplate(21,"RSVP for Automate the Meetup by MeetupMan")
if err:
    print(result)
else:
    print("Error parsing the template")
```